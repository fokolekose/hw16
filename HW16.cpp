﻿#include <iostream>
#include <time.h>

int main()
{
	const int size = 5;
	int array[size][size];
	int sum = 0;

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int line = buf.tm_mday % size;

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			array[i][j] = i + j;
			if (i == line)
			{
				sum += array[i][j];
			}
			std::cout << array[i][j] << " ";
		}
		std::cout << '\n';
	}
	std::cout << '\n' << "Day: " << buf.tm_mday << ", line: " 
		<< line + 1 << ", sum: " << sum << '\n';
}